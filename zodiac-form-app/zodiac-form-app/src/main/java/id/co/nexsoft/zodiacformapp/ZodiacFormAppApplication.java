package id.co.nexsoft.zodiacformapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZodiacFormAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZodiacFormAppApplication.class, args);
	}

}
