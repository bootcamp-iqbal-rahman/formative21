import { useEffect, useState } from "react";

const Fetch = (url) => {
    const [users, setUser] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/' + url)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                setUser(data);
            });
    }, []);

    return users;
};

export default Fetch;