export default function Card(props) {
    return (
        <>
            <div className="min-w-56 block rounded-lg bg-slate-200   p-6 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)]">
                <h1 className="mb-4 text-xl font-medium leading-tight text-neutral-800">{props.name}</h1>
                <h2 className="">{props.birthDate}</h2>
                <h2>{props.zodiac}</h2>
            </div>
        </>
    );
}