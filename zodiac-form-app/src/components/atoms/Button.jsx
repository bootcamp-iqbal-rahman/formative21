export default function Button(props) {
    return (
        <button
            className="btn"
            onClick={() => document.getElementById("add_data_modal").showModal()}>
            {props.text}
        </button>
    );
}