import Fetch from './FetchData';

const Calculate = ({birthDate}) => {
    const zodiacList = Fetch("zodiac");

    const searchZodiacByDate = (desiredDate) => {
        const desiredDateObj = new Date(desiredDate);
        const desiredMonth = desiredDateObj.getMonth();
        const desiredDay = desiredDateObj.getDate();
    
        const foundZodiac = zodiacList.find(zodiac => {
            const zodiacStartDate = new Date(zodiac.startDate);
            const zodiacEndDate = new Date(zodiac.endDate);
            const zodiacStartMonth = zodiacStartDate.getMonth();
            const zodiacStartDay = zodiacStartDate.getDate();
            const zodiacEndMonth = zodiacEndDate.getMonth();
            const zodiacEndDay = zodiacEndDate.getDate();
    
            return (desiredMonth > zodiacStartMonth || (desiredMonth === zodiacStartMonth && desiredDay >= zodiacStartDay)) &&
                    (desiredMonth < zodiacEndMonth || (desiredMonth === zodiacEndMonth && desiredDay <= zodiacEndDay));
        });

        return foundZodiac ? foundZodiac.id : null;
    };
    return searchZodiacByDate(birthDate);
}

export default Calculate;