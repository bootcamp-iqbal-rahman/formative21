import Input from '../atoms/Input'
import PostData from '../PostData'
import Calculate from '../CalculateZodiac';

export default function Modal(props) {
    const handleSubmit = async (e) => {
        e.preventDefault();
        const name = document.getElementById('add_name').value;
        const birthDate = document.getElementById('add_date').value;
        console.log(name, birthDate)
        const zodiac = await Calculate({birthDate});
        // console.log(zodiac)
        // PostData("user", { name, birthDate, zodiac }, e);
    };

    return (
        <dialog id="add_data_modal" className="modal modal-bottom sm:modal-middle">
            <div className="modal-box">
                <form onSubmit={handleSubmit}>
                    <Input 
                        id="add_name"
                        label="Name"
                        type="text"
                        value=""
                    />
                    <Input 
                        id="add_date"
                        label="Birth Date"
                        type="date"
                        value=""
                    />
                    <button
                        type="submit"
                        className="update"
                    >Add Data</button>
                </form>
                <div className="modal-action">
                    <form method="dialog">
                        <button className="btn">Close</button>
                    </form>
                </div>
            </div>
        </dialog>
    );
}