import React, { useEffect, useState } from 'react';
import Card from '../atoms/Card'
import Fetch from '../FetchData';

export default function CardColumn() {
    const [zodiacNames, setZodiacNames] = useState([]);

    const users = Fetch("user");
    const zodiacList = Fetch("zodiac");
    
    useEffect(() => {
        const names = users.map(user => {
            const zodiacItem = zodiacList.find(item => item.id === user.zodiacId);
            return zodiacItem ? zodiacItem.name : ''; // Return zodiac name or empty string if not found
        });
        setZodiacNames(names);
    }, [users, zodiacList]);

    return (
        <>
            <div className="justify-center flex flex-wrap -mx-1 lg:-mx-4 gap-4">
                {users.map((user, index) => (
                    <Card 
                        key={user.id}
                        name={user.name}
                        birthDate={user.birthDate}
                        zodiac={zodiacNames[index]}
                    />
                ))}
            </div>
        </>
    );
}