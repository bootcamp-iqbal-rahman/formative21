const addClick = async (url, data, e) => {
    e.preventDefault();

    await fetch('http://localhost:8080/api/' + url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then((response) => response.json())
    .then((data) => {
        console.log('Data successfully sent:', data);
    })
    .catch((error) => {
        console.error('Error:', error);
    });

    window.location.reload();
};

export default addClick;