import { useState } from 'react'
import './App.css'
import CardColumn from './components/molecules/CardColumn'
import Button from './components/atoms/Button'
import Modal from './components/molecules/Modal'

function App() {

  return (
    <>
      <div className="container my-12 mx-auto px-4 md:px-12">
        <CardColumn />
        <Button 
          text="Add Data"
        />
        <Modal />
      </div>
    </>
  )
}

export default App
