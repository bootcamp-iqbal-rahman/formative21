package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Zodiac;
import id.co.nexsoft.restapi.repository.ZodiacRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class ZodiacServiceImpl implements DefaultService<Zodiac> {
    @Autowired
    private ZodiacRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Zodiac> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Zodiac> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<Zodiac> Zodiac = repo.findById(id);
        if(!Zodiac.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Zodiac putData(Zodiac data, Long id) {
        Optional<Zodiac> Zodiac = repo.findById(id);
        if(!Zodiac.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Zodiac postData(Zodiac data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE zodiac SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}