package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    
}
