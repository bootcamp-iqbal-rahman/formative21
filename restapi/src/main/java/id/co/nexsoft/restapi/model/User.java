package id.co.nexsoft.restapi.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Name can not be null")
    @NotBlank(message = "Name can not be blank")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Birth date can not be null")
    @Column(name = "birth_date")
    private LocalDate birthDate;

    @NotNull(message = "Zodiac id can not be null")
    @Column(name = "zodiac_id")
    private int zodiacId;

    public User() {}

    public User(String name, LocalDate birthDate, int zodiacId) {
        this.name = name;
        this.birthDate = birthDate;
        this.zodiacId = zodiacId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getZodiacId() {
        return zodiacId;
    }

    public void setZodiacId(int zodiacId) {
        this.zodiacId = zodiacId;
    }
}
