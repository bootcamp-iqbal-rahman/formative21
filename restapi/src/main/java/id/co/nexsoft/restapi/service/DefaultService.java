package id.co.nexsoft.restapi.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DefaultService<T> {
    List<T> getAllData();
    Optional<T> getDataById(Long id);
    boolean deleteDataById(Long id);
    T putData(T data, Long id);
    T postData(T data);
    void patchData(Map<String, Object> data, Long id);
}
