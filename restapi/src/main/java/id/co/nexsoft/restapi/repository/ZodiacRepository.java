package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Zodiac;

public interface ZodiacRepository extends JpaRepository<Zodiac, Long> {
    
}
