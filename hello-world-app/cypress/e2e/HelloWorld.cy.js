describe('Hello World', () => {
  it('passes', () => {
    cy.visit('http://localhost:5173/')

    cy.get('h1').contains('Hello world!')
  })
})