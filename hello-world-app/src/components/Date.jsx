import React, { useState } from 'react';

export default function DateComponent() {
    const getDate = () => {
        return new Date();
    }
    
    const [currentDate] = useState(getDate());

    return (
        <div>
            {currentDate.toLocaleString()}
        </div>
    );
};