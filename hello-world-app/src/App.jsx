import './App.css'
import DateComponent from './components/Date'
import React from 'react';


function App() {
  return (
    <div className="text-3xl font-bold underline">
      <DateComponent />
      <h1>
        Hello world!
      </h1>
    </div>
  )
}

export default App